FROM alpine
EXPOSE 81

LABEL maintainer "dande"
LABEL description "Maxsold search"

# Copy python requirements file
COPY requirements.txt /tmp/requirements.txt

RUN apk add --no-cache \
    python3 \
    bash \
    nginx \
    uwsgi \
    uwsgi-python3 \
    supervisor && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    pip3 install wheel && \
    pip3 install cython && \
    pip3 install --no-use-pep517 -r /tmp/requirements.txt && \
    rm /etc/nginx/conf.d/default.conf; \
    rm -r /root/.cache


# Copy the Nginx global conf
COPY nginx.conf /etc/nginx/
# Copy the Flask Nginx site conf
COPY application-nginx.conf /etc/nginx/conf.d/
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY uwsgi.ini /etc/uwsgi/
# Custom Supervisord config
COPY supervisord.conf /etc/supervisord.conf

# Copy app files and set permissions
COPY ./app /app
RUN chown -R nginx.nginx /app
WORKDIR /app

CMD ["/usr/bin/supervisord"]
