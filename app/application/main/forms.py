from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, IntegerField

class HomeForm(FlaskForm):
    search_term=StringField('Search for')
    radius = IntegerField('Radius (km)', default=200)
    location_field = StringField('Location (lat/lon)', default="43.65189362, -79.38171387")
    search_button=SubmitField('Search')