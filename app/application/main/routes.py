from flask import (
    render_template, current_app
)
import requests
import re
from datetime import datetime

import pytz

from application.main import main_bp
from application.main.forms import HomeForm




@main_bp.route('/', methods=['GET', 'POST'])
def index():
    form = HomeForm()
    auction_items = list()
    ms_url="https://maxsold.api.maxsold.com/algolia/search"
    ms_api_key="c4e2c29964993277b9d90061957cff81"
    ms_origin="https://maxsold.com"
    ms_body= {
    
    "selectedFilters": {
        "coordinates": {
            "lat": 43.65189362,
            "lng": -79.38171387
        },
        "distance": 200,
        "searchText": "",
        "address": "Britt, ON, Canada",
        "urlComponents": {
            "country": "canada",
            "state": "ontario",
            "region": "toronto",
            "auctionType": "",
            "category": "",
            "subCategory": []
        }
    },
    "algoliaIndexName": "hpitem",
    "hitsPerPage": 1000,
    "page": 0

    }
    ms_headers = {
        "x-api-key": ms_api_key,
        "Origin": ms_origin
    }



    if form.validate_on_submit():
        ms_body['selectedFilters']['searchText'] = form.search_term.data
        if form.radius.data > 0:
            ms_body['selectedFilters']['distance'] = form.radius.data * 1
        if form.location_field.data:
            ms_body['selectedFilters']['coordinates']['lat'] = float(form.location_field.data.split(',')[0])
            ms_body['selectedFilters']['coordinates']['lng'] = float(form.location_field.data.split(',')[1])
        r = requests.post(url=ms_url, headers=ms_headers, json=ms_body)
        response = r.json()
        
        auction_items = sorted(response['hits'], key=lambda d: d['end_date'])
        today = datetime.now().astimezone(pytz.timezone(current_app.config['TZ_LOCAL']))
        for item in auction_items:
            title = re.sub(r'[^A-Za-z ]+', '', item['title']).split()
            title.append(item['objectID'])
            item['item_link'] = '-'.join(title)
            end_date = datetime.fromtimestamp(item['end_date']).astimezone(pytz.timezone(current_app.config['TZ_LOCAL']))
            item['end_date'] = end_date
            item['ends_today'] = True if end_date.date() == today.date() else False



    return render_template(
        'index.html',
        title = 'Maxsold search',
        auction_items=auction_items,
        form = form
    )
