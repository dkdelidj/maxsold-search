$(document).ready(function () {
  var location_field = document.getElementById("location");

  $('.item-img').on('click tap', function(e) {
    $(this).toggleClass('item-img')
  })
});

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
  }
}

function showPosition(position) {
  location_field.value = position.coords.latitude + ", " + position.coords.longitude;
}
