from flask import Flask

import logging
from logging.handlers import RotatingFileHandler
import os

from config import Config



def create_app(config_class=Config):
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_class)
    with app.app_context():

        from application.errors import errors_bp
        app.register_blueprint(errors_bp)

        from application.main import main_bp
        app.register_blueprint(main_bp)

        if not app.debug:
            if not os.path.exists('logs'):
                os.mkdir('logs')
            file_handler = RotatingFileHandler('logs/access.log', maxBytes=10240,
                                               backupCount=10)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)

            app.logger.setLevel(logging.INFO)
            app.logger.info('Startup')
    
        return app