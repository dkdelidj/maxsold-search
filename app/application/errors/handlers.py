from flask import render_template, jsonify
from application.errors import errors_bp
from application.errors.custom_exceptions import InvalidUsage

@errors_bp.app_errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@errors_bp.app_errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500

@errors_bp.app_errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
