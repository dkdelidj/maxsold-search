from flask import Blueprint

errors_bp = Blueprint('errors', __name__
                    , template_folder='templates'
                    , static_folder='static'
                    )

from application.errors import handlers
